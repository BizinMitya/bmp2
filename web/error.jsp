<%@ page import="java.util.Arrays" %><%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 04.04.2016
  Time: 22:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page isErrorPage="true" %>
<html>
<head>
    <title>Ошибка</title>
</head>
<body>
<%
    if(exception.getMessage().equals("Нет такого id!")){
        out.print("<h1>Нет такого id в базе! Вернуться <a href=\"index.jsp\">назад</a>.</h1>");
    }else if(exception.getMessage().equals("Нет такого имени!")) {
        out.print("<h1>Нет такого имени в базе! Вернуться <a href=\"index.jsp\">назад</a>.</h1>");
    }
    else if(exception.getMessage().equals("База пуста!")) {
        out.print("<h1>База пуста! Вернуться <a href=\"index.jsp\">назад</a>.</h1>");
    }
    else{
        out.print("<h1>Возникла ошибка ввода имени или id! Вернуться <a href=\"index.jsp\">назад</a>.</h1>");
        exception.printStackTrace();
    }
%>
</body>
</html>
