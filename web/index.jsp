<%@ page import="employee.Employee" %>
<%@ page import="employee.EmployeeHome" %>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="java.util.List" %>
<%@ page import="javax.ejb.ObjectNotFoundException" %>
<%--
  Created by IntelliJ IDEA.
  User: Dmitriy
  Date: 04.04.2016
  Time: 22:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page errorPage="error.jsp" %>
<html>
<head>
    <script type="text/javascript" src="javascript/jquery-2.2.2.js"></script>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <meta charset="utf-8">
    <title>Сотрудники</title>
</head>
<body>
<form name="number_form" action="index.jsp" method="post">
    <p>Введите номер сотрудника:</p>
    <input class="input_text" name="idQuery" type="text" size="10">
    <input class="button" type="submit" value="Отправить">
    <script type="text/javascript">
        number_form.onsubmit = function () {
            var number = number_form.elements.number.value;
            if (!number.match(/^\d+$/)) {
                alert("Введите целое число!");
            }
        }
    </script>
</form>
<form name="name_form" action="index.jsp" method="post">
    <p>Введите имя сотрудника:</p>
    <input class="input_text" id="input_name" value="например, Иванов" name="nameQuery" type="text" size="10">
    <script type="text/javascript">
        input_name.onfocus = function () {
            if (input_name.value == "например, Иванов") {
                input_name.value = "";
            }
        }
        input_name.onblur = function () {
            if (input_name.value == "") {
                input_name.value = "например, Иванов";
            }
        }
        name_form.onsubmit = function () {
            window.open("save.jsp?name=" + encodeURIComponent(input_name.value));
        }
    </script>
    </input>
    <input class="button" type="submit" value="Отправить">
</form>
<p>Вывести всю информацию о сотрудниках:
<form action="index.jsp" method="post"><input class="button" name="allQuery" type="submit" value="Вывести"></form>
</p>
<a href="new.html">Создать нового сотрудника</a>
<p></p>
<table id="tableOfEmployees">
    <script type="text/javascript">
        function deleteEmployee(row) {
            if (confirm("Удалить?")) {
                document.getElementById('tableOfEmployees').deleteRow(row.parentNode.parentNode.rowIndex);
            }
        }
    </script>
    <tr>
        <th style="font: bold 14pt/12pt Verdana;" colspan="7">Сотрудники</th>
    </tr>
    <tr>
        <th></th>
        <th style="font: bold 12pt/10pt Verdana;">Номер сотрудника</th>
        <th style="font: bold 12pt/10pt Verdana;">Имя сотрудника</th>
        <th style="font: bold 12pt/10pt Verdana;">Должность</th>
        <th style="font: bold 12pt/10pt Verdana;">Дата принятия на работу</th>
        <th style="font: bold 12pt/10pt Verdana;">Департамент</th>
        <th style="font: bold 12pt/10pt Verdana;">"Удалить"</th>
    </tr>
    <%!
        public static EmployeeHome getEmployeeHome() throws NamingException {
            InitialContext initialContext = new InitialContext();
            return (EmployeeHome) initialContext.lookup("ejb:BMP2_ear_exploded/ejb/EmployeeBean!employee.EmployeeHome");
        }
    %>
    <%
        String idQuery = request.getParameter("idQuery");
        String allQuery = request.getParameter("allQuery");
        String nameQuery = request.getParameter("nameQuery");
        if (request.getMethod().equalsIgnoreCase("POST")) {
            if (idQuery != null) {
                try {
                    Employee employee = getEmployeeHome().findByPrimaryKey(Integer.valueOf(idQuery));
                    if (employee == null) {
                        throw new Exception("Нет такого id!");
                    } else {
                        out.print("<tr><th><input type=\"checkbox\"/></th>" +
                                "<th><a href=\"\">" + employee.getEmpno() + "</a></th>" +
                                "<th>" + employee.getEname() + "</th>" +
                                "<th>" + employee.getJob() + "</th>" +
                                "<th>" + employee.getHiredate() + "</th>" +
                                "<th>" + employee.getDeptno() + "</th>" +
                                "<th><a href=\"\" onclick=\"deleteEmployee(this);return false;\">Удалить</a></th></tr>");
                    }
                } catch (NumberFormatException | ObjectNotFoundException e) {
                    throw new Exception("Нет такого id!");
                }
            }
            if (nameQuery != null) {
                List<Employee> list = (List<Employee>) getEmployeeHome().findByEname(nameQuery);
                if (list.size() == 0) {
                    throw new Exception("Нет такого имени!");
                } else {
                    for (Employee employee : list) {
                        out.print("<tr><th><input type=\"checkbox\"/></th>" +
                                "<th><a href=\"\">" + employee.getEmpno() + "</a></th>" +
                                "<th>" + employee.getEname() + "</th>" +
                                "<th>" + employee.getJob() + "</th>" +
                                "<th>" + employee.getHiredate() + "</th>" +
                                "<th>" + employee.getDeptno() + "</th>" +
                                "<th><a href=\"\" onclick=\"deleteEmployee(this);return false;\">Удалить</a></th></tr>");
                    }
                }
            }
            if (allQuery != null) {
                List<Employee> employees = (List<Employee>) getEmployeeHome().findAll();
                if (employees.size() == 0) {
                    throw new Exception("База пуста!");
                } else {
                    for (Employee employee : employees) {
                        out.print("<tr><th><input type=\"checkbox\"/></th>" +
                                "<th><a href=\"\">" + employee.getEmpno() + "</a></th>" +
                                "<th>" + employee.getEname() + "</th>" +
                                "<th>" + employee.getJob() + "</th>" +
                                "<th>" + employee.getHiredate() + "</th>" +
                                "<th>" + employee.getDeptno() + "</th>" +
                                "<th><a href=\"\" onclick=\"deleteEmployee(this);return false;\">Удалить</a></th></tr>");
                    }
                }
            }
        }
    %>
</table>
</body>
</html>
