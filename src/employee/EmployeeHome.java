package employee;

import javax.ejb.EJBHome;
import javax.ejb.FinderException;
import java.rmi.RemoteException;
import java.util.Collection;

/**
 * Created by Dmitriy on 17.04.2016.
 */
public interface EmployeeHome extends EJBHome {
    Employee findByPrimaryKey(Integer empno) throws RemoteException, FinderException;

    Collection findByEname(String ename) throws FinderException, RemoteException;

    Collection findAll() throws FinderException, RemoteException;

}
