package employee;

import javax.ejb.EntityBean;
import javax.ejb.EntityContext;
import javax.ejb.NoSuchEntityException;
import javax.ejb.ObjectNotFoundException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Dmitriy on 17.04.2016.
 */
public class EmployeeBean implements EntityBean {

    private Integer empno;
    private String ename;
    private String job;
    private String hiredate;
    private Integer deptno;
    private EntityContext entityContext;
    private DataSource dataSource;

    public Integer ejbFindByPrimaryKey(Integer empno) throws ObjectNotFoundException {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT `empno` FROM `netcracker`.`emp` WHERE empno = ?");
            preparedStatement.setInt(1, empno);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                throw new ObjectNotFoundException();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return empno;
    }

    public Collection ejbFindByEname(String ename) {
        List<Integer> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT `empno` FROM `netcracker`.`emp` WHERE ename = ?");
            preparedStatement.setString(1, ename);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Collection ejbFindAll() {
        List<Integer> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT `empno` FROM `netcracker`.`emp`");
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                list.add(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public Integer getEmpno() {
        return empno;
    }

    public void setEmpno(Integer empno) {
        this.empno = empno;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getHiredate() {
        return hiredate;
    }

    public void setHiredate(String hiredate) {
        this.hiredate = hiredate;
    }

    public Integer getDeptno() {
        return deptno;
    }

    public void setDeptno(Integer deptno) {
        this.deptno = deptno;
    }


    @Override
    public void setEntityContext(EntityContext entityContext) {
        this.entityContext = entityContext;
        Context context = null;
        try {
            context = new InitialContext();
            dataSource = (DataSource) context.lookup("java:/MySqlDS");
        } catch (NamingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void unsetEntityContext() {
        this.entityContext = null;
    }

    @Override
    public void ejbRemove() {

    }

    @Override
    public void ejbActivate() {

    }

    @Override
    public void ejbPassivate() {

    }

    @Override
    public void ejbLoad() {
        empno = (Integer) entityContext.getPrimaryKey();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT `ename`,`job`,`hiredate`,`deptno` FROM `netcracker`.`emp` WHERE empno = ?");
            preparedStatement.setInt(1, empno);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
                throw new NoSuchEntityException();
            }
            ename = resultSet.getString(1);
            job = resultSet.getString(2);
            hiredate = resultSet.getString(3);
            deptno = resultSet.getInt(4);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void ejbStore() {

    }
}
